package com.ezimaxtechnologies.automotocast;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ezimaxtechnologies.automotocast.MainActivity;
import com.ezimaxtechnologies.automotocast.R;
import com.ezimaxtechnologies.automotocast.singletonClass.Singleton_Session;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by pc on 24-05-2017.
 */

public class ProfileDetails extends Activity {
    EditText firstname, lastname, username;
    String first_name, last_name, user_name;
    Button cancel_btn;
    ImageView arrow;
    Button change_avatar,popup_cancel;
    LinearLayout camera_gallery_block;
    RelativeLayout selectpic_layout;



    int userId;
    String baseUrl;
    int hashkey_avatar;
    ImageView getAvatar_img;
    byte[] avatar_response;
    RelativeLayout access_gallery;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        firstname = (EditText) findViewById(R.id.Edittxt_firstname);
        lastname = (EditText) findViewById(R.id.Edittxt_lastname);
        username = (EditText) findViewById(R.id.Edittxt_username);
        cancel_btn = (Button)findViewById(R.id.cancel_action);
        arrow = (ImageView)findViewById(R.id.backarrow);
        change_avatar = (Button)findViewById(R.id.Changeavatar);
        access_gallery = (RelativeLayout)findViewById(R.id.access_gallery);
        popup_cancel = (Button)findViewById(R.id.cancel_popup);
        camera_gallery_block = (LinearLayout)findViewById(R.id.camera_gallery_block);
        selectpic_layout = (RelativeLayout)findViewById(R.id.selectpic_layout);

        selectpic_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera_gallery_block.setVisibility(View.VISIBLE);
            }
        });


        popup_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                access_gallery.setVisibility(View.GONE);

            }
        });


        change_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            access_gallery.setVisibility(View.VISIBLE);

            }
        });


        userId = Singleton_Session.Instance().userId;
        baseUrl = Singleton_Session.Instance().baseUrl;

        getAvatar_img = (CircleImageView) findViewById(R.id.avatar);

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileDetails.this, MainActivity.class);
                startActivity(intent);
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileDetails.this, MainActivity.class);
                startActivity(intent);
            }
        });

        new fetchDetails().execute();
    }

    /////////////////////////
    /// User Profile Details ///
    ////////////////////////

    private class fetchDetails extends AsyncTask<Void, Void, Void> {
        public int code;
        public String responseBody;
        Gson authentication;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {


            OkHttpClient client = new OkHttpClient();
            Log.d("Do in background maigya", "");
            Request request = new Request.Builder()
                    .url("http://app.automotocast.ca/api/user/profile")
                    .get()
//                    .addHeader("X-CSRF-TOKEN", Singleton_Session.Instance().token)
                    .addHeader("cache-control", "no-cache")
                    .addHeader("cookie", Singleton_Session.Instance().cookie)
//                    .addHeader("postman-token", )
                    .build();

            Log.d("REQUEST :", request.toString());


            Response response = null;
            try {
                response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d("PROFILE RESPONSE :", responseBody);

                try {
                    JSONObject json = new JSONObject(responseBody);
                    JSONObject json_LL = json.getJSONObject("result");
                    first_name = json_LL.getString("firstName");
                    last_name = json_LL.getString("lastName");
                    user_name = json_LL.getString("username");
                    Log.d("FIRST NAME", first_name);
                    Log.d("LAST NAME", last_name);
                    Log.d("USER NAME", user_name);
                    hashkey_avatar = json_LL.getInt("avatar");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {


            firstname.setText(first_name);
            lastname.setText(last_name);
            username.setText(user_name);

            new fetchAvatar().execute();
            super.onPostExecute(aVoid);
        }

    }


    class fetchAvatar extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... params) {


            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(baseUrl + "/files/user/avatar/" + userId + "/" + hashkey_avatar)
                    .get()
                    .addHeader("cache-control", "no-cache")
                    .addHeader("cookie", Singleton_Session.Instance().cookie)
                    .addHeader("postman-token", "47e4a2cc-514b-61a7-316f-4006aa1639de")
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                avatar_response = response.body().bytes();
                Log.d("avatar_response", avatar_response.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Bitmap bitmap = BitmapFactory.decodeByteArray(avatar_response, 0, avatar_response.length);
            getAvatar_img.setImageBitmap(bitmap);
            super.onPostExecute(aVoid);
        }


    }
}
