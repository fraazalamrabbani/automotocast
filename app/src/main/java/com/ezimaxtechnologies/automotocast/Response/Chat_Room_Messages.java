package com.ezimaxtechnologies.automotocast.Response;

import java.util.List;

/**
 * Created by fraazalam on 23/05/17.
 */

public class Chat_Room_Messages {


    /**
     * result : {"roomEvents":[{"id":244406,"room":{"roomType":"PUBLIC","id":142590},"data":[{"message":"new broadcast","tags":["Buy"],"chatterFoxFiles":[{"id":244431,"createDate":1493748101263,"version":0,"lastModifiedDate":null,"mongoObjectId":"5908c985c9fa2901171b684c","mongoThumbnailObjectId":"5908c985c9fa2901171b684a","name":"undefined","type":"image/png","size":95917}],"linkMetadata":null,"removed":false,"replyCount":0}],"initiator":{"deleted":false,"firstName":"Derek","lastName":"Rathbun","lastLoginDate":1494435476620,"accountLocked":false,"avatar":"589fc05467b1af48925da9fb","userRole":"CHAT_USER","username":"derek@datton.ca","fullName":"Derek Rathbun","id":4691,"$version":979,"userGroup":{"address":{"street":"5364 Spetifore Crescent","city":"Delta","subdivision":"CA_BC","postalCode":"V4M4H6","regionAbbrievation":"BC","country":"Canada","region":"British Columbia"},"name":"Industry Cast Admin","id":30},"retail":false,"$notifyBroadcastMessages":true,"$lastWebsocketDate":1495606850792},"date":1493748101353,"roomEventType":"BROADCAST"}],"$lastMessage":true}
     * status : OK
     * statusCode : 200
     */

    private ResultBean result;
    private String status;
    private int statusCode;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class ResultBean {
        /**
         * roomEvents : [{"id":244406,"room":{"roomType":"PUBLIC","id":142590},"data":[{"message":"new broadcast","tags":["Buy"],"chatterFoxFiles":[{"id":244431,"createDate":1493748101263,"version":0,"lastModifiedDate":null,"mongoObjectId":"5908c985c9fa2901171b684c","mongoThumbnailObjectId":"5908c985c9fa2901171b684a","name":"undefined","type":"image/png","size":95917}],"linkMetadata":null,"removed":false,"replyCount":0}],"initiator":{"deleted":false,"firstName":"Derek","lastName":"Rathbun","lastLoginDate":1494435476620,"accountLocked":false,"avatar":"589fc05467b1af48925da9fb","userRole":"CHAT_USER","username":"derek@datton.ca","fullName":"Derek Rathbun","id":4691,"$version":979,"userGroup":{"address":{"street":"5364 Spetifore Crescent","city":"Delta","subdivision":"CA_BC","postalCode":"V4M4H6","regionAbbrievation":"BC","country":"Canada","region":"British Columbia"},"name":"Industry Cast Admin","id":30},"retail":false,"$notifyBroadcastMessages":true,"$lastWebsocketDate":1495606850792},"date":1493748101353,"roomEventType":"BROADCAST"}]
         * $lastMessage : true
         */

        private boolean $lastMessage;
        private List<RoomEventsBean> roomEvents;

        public boolean is$lastMessage() {
            return $lastMessage;
        }

        public void set$lastMessage(boolean $lastMessage) {
            this.$lastMessage = $lastMessage;
        }

        public List<RoomEventsBean> getRoomEvents() {
            return roomEvents;
        }

        public void setRoomEvents(List<RoomEventsBean> roomEvents) {
            this.roomEvents = roomEvents;
        }

        public static class RoomEventsBean {
            /**
             * id : 244406
             * room : {"roomType":"PUBLIC","id":142590}
             * data : [{"message":"new broadcast","tags":["Buy"],"chatterFoxFiles":[{"id":244431,"createDate":1493748101263,"version":0,"lastModifiedDate":null,"mongoObjectId":"5908c985c9fa2901171b684c","mongoThumbnailObjectId":"5908c985c9fa2901171b684a","name":"undefined","type":"image/png","size":95917}],"linkMetadata":null,"removed":false,"replyCount":0}]
             * initiator : {"deleted":false,"firstName":"Derek","lastName":"Rathbun","lastLoginDate":1494435476620,"accountLocked":false,"avatar":"589fc05467b1af48925da9fb","userRole":"CHAT_USER","username":"derek@datton.ca","fullName":"Derek Rathbun","id":4691,"$version":979,"userGroup":{"address":{"street":"5364 Spetifore Crescent","city":"Delta","subdivision":"CA_BC","postalCode":"V4M4H6","regionAbbrievation":"BC","country":"Canada","region":"British Columbia"},"name":"Industry Cast Admin","id":30},"retail":false,"$notifyBroadcastMessages":true,"$lastWebsocketDate":1495606850792}
             * date : 1493748101353
             * roomEventType : BROADCAST
             */

            private int id;
            private RoomBean room;
            private InitiatorBean initiator;
            private long date;
            private String roomEventType;
            private List<DataBean> data;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public RoomBean getRoom() {
                return room;
            }

            public void setRoom(RoomBean room) {
                this.room = room;
            }

            public InitiatorBean getInitiator() {
                return initiator;
            }

            public void setInitiator(InitiatorBean initiator) {
                this.initiator = initiator;
            }

            public long getDate() {
                return date;
            }

            public void setDate(long date) {
                this.date = date;
            }

            public String getRoomEventType() {
                return roomEventType;
            }

            public void setRoomEventType(String roomEventType) {
                this.roomEventType = roomEventType;
            }

            public List<DataBean> getData() {
                return data;
            }

            public void setData(List<DataBean> data) {
                this.data = data;
            }

            public static class RoomBean {
                /**
                 * roomType : PUBLIC
                 * id : 142590
                 */

                private String roomType;
                private int id;

                public String getRoomType() {
                    return roomType;
                }

                public void setRoomType(String roomType) {
                    this.roomType = roomType;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }
            }

            public static class InitiatorBean {
                /**
                 * deleted : false
                 * firstName : Derek
                 * lastName : Rathbun
                 * lastLoginDate : 1494435476620
                 * accountLocked : false
                 * avatar : 589fc05467b1af48925da9fb
                 * userRole : CHAT_USER
                 * username : derek@datton.ca
                 * fullName : Derek Rathbun
                 * id : 4691
                 * $version : 979
                 * userGroup : {"address":{"street":"5364 Spetifore Crescent","city":"Delta","subdivision":"CA_BC","postalCode":"V4M4H6","regionAbbrievation":"BC","country":"Canada","region":"British Columbia"},"name":"Industry Cast Admin","id":30}
                 * retail : false
                 * $notifyBroadcastMessages : true
                 * $lastWebsocketDate : 1495606850792
                 */

                private boolean deleted;
                private String firstName;
                private String lastName;
                private long lastLoginDate;
                private boolean accountLocked;
                private String avatar;
                private String userRole;
                private String username;
                private String fullName;
                private int id;
                private int $version;
                private UserGroupBean userGroup;
                private boolean retail;
                private boolean $notifyBroadcastMessages;
                private long $lastWebsocketDate;

                public boolean isDeleted() {
                    return deleted;
                }

                public void setDeleted(boolean deleted) {
                    this.deleted = deleted;
                }

                public String getFirstName() {
                    return firstName;
                }

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public String getLastName() {
                    return lastName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public long getLastLoginDate() {
                    return lastLoginDate;
                }

                public void setLastLoginDate(long lastLoginDate) {
                    this.lastLoginDate = lastLoginDate;
                }

                public boolean isAccountLocked() {
                    return accountLocked;
                }

                public void setAccountLocked(boolean accountLocked) {
                    this.accountLocked = accountLocked;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getUserRole() {
                    return userRole;
                }

                public void setUserRole(String userRole) {
                    this.userRole = userRole;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getFullName() {
                    return fullName;
                }

                public void setFullName(String fullName) {
                    this.fullName = fullName;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int get$version() {
                    return $version;
                }

                public void set$version(int $version) {
                    this.$version = $version;
                }

                public UserGroupBean getUserGroup() {
                    return userGroup;
                }

                public void setUserGroup(UserGroupBean userGroup) {
                    this.userGroup = userGroup;
                }

                public boolean isRetail() {
                    return retail;
                }

                public void setRetail(boolean retail) {
                    this.retail = retail;
                }

                public boolean is$notifyBroadcastMessages() {
                    return $notifyBroadcastMessages;
                }

                public void set$notifyBroadcastMessages(boolean $notifyBroadcastMessages) {
                    this.$notifyBroadcastMessages = $notifyBroadcastMessages;
                }

                public long get$lastWebsocketDate() {
                    return $lastWebsocketDate;
                }

                public void set$lastWebsocketDate(long $lastWebsocketDate) {
                    this.$lastWebsocketDate = $lastWebsocketDate;
                }

                public static class UserGroupBean {
                    /**
                     * address : {"street":"5364 Spetifore Crescent","city":"Delta","subdivision":"CA_BC","postalCode":"V4M4H6","regionAbbrievation":"BC","country":"Canada","region":"British Columbia"}
                     * name : Industry Cast Admin
                     * id : 30
                     */

                    private AddressBean address;
                    private String name;
                    private int id;

                    public AddressBean getAddress() {
                        return address;
                    }

                    public void setAddress(AddressBean address) {
                        this.address = address;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public static class AddressBean {
                        /**
                         * street : 5364 Spetifore Crescent
                         * city : Delta
                         * subdivision : CA_BC
                         * postalCode : V4M4H6
                         * regionAbbrievation : BC
                         * country : Canada
                         * region : British Columbia
                         */

                        private String street;
                        private String city;
                        private String subdivision;
                        private String postalCode;
                        private String regionAbbrievation;
                        private String country;
                        private String region;

                        public String getStreet() {
                            return street;
                        }

                        public void setStreet(String street) {
                            this.street = street;
                        }

                        public String getCity() {
                            return city;
                        }

                        public void setCity(String city) {
                            this.city = city;
                        }

                        public String getSubdivision() {
                            return subdivision;
                        }

                        public void setSubdivision(String subdivision) {
                            this.subdivision = subdivision;
                        }

                        public String getPostalCode() {
                            return postalCode;
                        }

                        public void setPostalCode(String postalCode) {
                            this.postalCode = postalCode;
                        }

                        public String getRegionAbbrievation() {
                            return regionAbbrievation;
                        }

                        public void setRegionAbbrievation(String regionAbbrievation) {
                            this.regionAbbrievation = regionAbbrievation;
                        }

                        public String getCountry() {
                            return country;
                        }

                        public void setCountry(String country) {
                            this.country = country;
                        }

                        public String getRegion() {
                            return region;
                        }

                        public void setRegion(String region) {
                            this.region = region;
                        }
                    }
                }
            }

            public static class DataBean {
                /**
                 * message : new broadcast
                 * tags : ["Buy"]
                 * chatterFoxFiles : [{"id":244431,"createDate":1493748101263,"version":0,"lastModifiedDate":null,"mongoObjectId":"5908c985c9fa2901171b684c","mongoThumbnailObjectId":"5908c985c9fa2901171b684a","name":"undefined","type":"image/png","size":95917}]
                 * linkMetadata : null
                 * removed : false
                 * replyCount : 0
                 */

                private String message;
                private Object linkMetadata;
                private boolean removed;
                private int replyCount;
                private List<String> tags;
                private List<ChatterFoxFilesBean> chatterFoxFiles;

                public String getMessage() {
                    return message;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public Object getLinkMetadata() {
                    return linkMetadata;
                }

                public void setLinkMetadata(Object linkMetadata) {
                    this.linkMetadata = linkMetadata;
                }

                public boolean isRemoved() {
                    return removed;
                }

                public void setRemoved(boolean removed) {
                    this.removed = removed;
                }

                public int getReplyCount() {
                    return replyCount;
                }

                public void setReplyCount(int replyCount) {
                    this.replyCount = replyCount;
                }

                public List<String> getTags() {
                    return tags;
                }

                public void setTags(List<String> tags) {
                    this.tags = tags;
                }

                public List<ChatterFoxFilesBean> getChatterFoxFiles() {
                    return chatterFoxFiles;
                }

                public void setChatterFoxFiles(List<ChatterFoxFilesBean> chatterFoxFiles) {
                    this.chatterFoxFiles = chatterFoxFiles;
                }

                public static class ChatterFoxFilesBean {
                    /**
                     * id : 244431
                     * createDate : 1493748101263
                     * version : 0
                     * lastModifiedDate : null
                     * mongoObjectId : 5908c985c9fa2901171b684c
                     * mongoThumbnailObjectId : 5908c985c9fa2901171b684a
                     * name : undefined
                     * type : image/png
                     * size : 95917
                     */

                    private int id;
                    private long createDate;
                    private int version;
                    private Object lastModifiedDate;
                    private String mongoObjectId;
                    private String mongoThumbnailObjectId;
                    private String name;
                    private String type;
                    private int size;

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public long getCreateDate() {
                        return createDate;
                    }

                    public void setCreateDate(long createDate) {
                        this.createDate = createDate;
                    }

                    public int getVersion() {
                        return version;
                    }

                    public void setVersion(int version) {
                        this.version = version;
                    }

                    public Object getLastModifiedDate() {
                        return lastModifiedDate;
                    }

                    public void setLastModifiedDate(Object lastModifiedDate) {
                        this.lastModifiedDate = lastModifiedDate;
                    }

                    public String getMongoObjectId() {
                        return mongoObjectId;
                    }

                    public void setMongoObjectId(String mongoObjectId) {
                        this.mongoObjectId = mongoObjectId;
                    }

                    public String getMongoThumbnailObjectId() {
                        return mongoThumbnailObjectId;
                    }

                    public void setMongoThumbnailObjectId(String mongoThumbnailObjectId) {
                        this.mongoThumbnailObjectId = mongoThumbnailObjectId;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getType() {
                        return type;
                    }

                    public void setType(String type) {
                        this.type = type;
                    }

                    public int getSize() {
                        return size;
                    }

                    public void setSize(int size) {
                        this.size = size;
                    }
                }
            }
        }
    }
}
