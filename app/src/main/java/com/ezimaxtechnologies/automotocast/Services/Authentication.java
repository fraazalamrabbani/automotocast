package com.ezimaxtechnologies.automotocast.Services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

//import com.ezimaxtechnologies.automotocast.LoginScreen;
import com.ezimaxtechnologies.automotocast.MainActivity;
import com.ezimaxtechnologies.automotocast.Response.User_Authentication;
import com.ezimaxtechnologies.automotocast.Response.User_Profile;
import com.ezimaxtechnologies.automotocast.singletonClass.Singleton_Session;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Authentication extends Service {

    private final IBinder authenticationBinder = new AuthenticationBinder();
    public String baseUrl = Singleton_Session.Instance().baseUrl;
    public String token, emailId, passwordValue, cookie;
    public boolean validEmail;
    //public LoginScreen loginScreen = new LoginScreen();
    User_Authentication authentication_response;
    User_Profile resp_obj_user_profile;

    //ProgressBar loginProgress = (ProgressBar) loginScreen.findViewById(R.id.loginProgress);

    public Authentication() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return authenticationBinder;
    }


    public boolean getAuth(){
            //task to do here..
        new getToken().execute();
        //Log.d("token",token);
        return true;
    }

    public class AuthenticationBinder extends Binder{

        public Authentication getService(){

            return Authentication.this;
        }

    }







    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //////////////////////////
    ///// Get CSRF-TOKEN /////
    /////////////////////////

    private class getToken extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }



        @Override
        protected Void doInBackground(Void... params) {

            try {
                URL url = new URL(baseUrl);
                HttpURLConnection getToken = (HttpURLConnection) url.openConnection();
                getToken.setRequestMethod("HEAD");
                getToken.connect();
                token = getToken.getHeaderField("X-CSRF-TOKEN");
                Singleton_Session.Instance().token=token;

            }
            catch (Exception e){


            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            emailId = Singleton_Session.Instance().email;
            passwordValue = Singleton_Session.Instance().password;
            validEmail = isValidEmail(emailId);

            if(validEmail){
                if(token!=null){
                    new checkAuth().execute();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Server not responding.",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(getApplicationContext(),"Invalid EmailId!",Toast.LENGTH_SHORT).show();
               // loginScreen.disableProgress();
            }
        }
    }


    //////////////////////////
    /// Authenticate User ///
    /////////////////////////

    private class checkAuth extends AsyncTask<Void,Void,Void>{

        public int code;
        public String message;
        Gson authentication;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\n \"username\": \"" + emailId + "\",\n \"password\": \"" + passwordValue + "\",\n \"cf_area\": \"CHAT\",\n \"rememberMe\": true\n}");
            Request request = new Request.Builder()
                    .url(baseUrl+"/json_security_check")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("X-CSRF-TOKEN", token)
                    .addHeader("X-CSRF-HEADER", "X-CSRF-TOKEN")
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                String responseBody =response.body().string();
                cookie = response.header("Set-Cookie");
                Singleton_Session.Instance().cookie = cookie;

                authentication = new Gson();
                authentication_response = authentication.fromJson(responseBody,User_Authentication.class);


            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(authentication_response != null){
                boolean autenticationValue = authentication_response.isAuthenticated();
                if(autenticationValue){
                    new getProfile().execute();
                }else {
                    Toast.makeText(getApplicationContext(),authentication_response.getReason(),Toast.LENGTH_LONG).show();
                    //loginScreen.disableProgress();
                }
            }

        }
    }


    /////////////////////////
    /// Get User Profile ///
    ////////////////////////

    private class getProfile extends AsyncTask<Void, Void, Void>{

        Gson gson;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected Void doInBackground(Void... params) {


            OkHttpClient profile = new OkHttpClient();

            Request profilerequest = new Request.Builder()
                    .url(baseUrl+"/user/profile")
                    .get()
                    .addHeader("cache-control", "no-cache")
                    .addHeader("cookie", cookie)
                    .build();

            Response response = null;
            try {
                response = profile.newCall(profilerequest).execute();
                String responseBody = response.body().string();
                gson=new Gson();
                resp_obj_user_profile= gson.fromJson(responseBody, User_Profile.class);
                //Log.d("body",responseBody);

            }catch (Exception e){


            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if(resp_obj_user_profile!=null) {
                int id = resp_obj_user_profile.getResult().getId();
                Singleton_Session.Instance().userId=id;
                Singleton_Session.Instance().userName = resp_obj_user_profile.getResult().getFullName();
                Singleton_Session.Instance().userAvatar = resp_obj_user_profile.getResult().getAvatar();
                Singleton_Session.Instance().fullDescription = resp_obj_user_profile.getResult().getFullName()+", "+resp_obj_user_profile.getResult().getUserGroup().getName()+" - "+resp_obj_user_profile.getResult().getUserGroup().getAddress().getCity()+", "+resp_obj_user_profile.getResult().getUserGroup().getAddress().getRegionAbbrievation();
                Intent loginIntent = new Intent(getApplicationContext(), MainActivity.class);
                //loginScreen.disableProgress();
                startActivity(loginIntent);
            }
        }



    }








}
