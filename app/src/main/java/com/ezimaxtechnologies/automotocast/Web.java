package com.ezimaxtechnologies.automotocast;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;

/**
 * Created by pc on 25-05-2017.
 */


public class Web extends Activity {


    private WebView myWebView;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Web.this,MainActivity.class);
        startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_layout);
        String url ="http://automotocast.ca/";
        myWebView = (WebView)this.findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl(url);



    }

}
